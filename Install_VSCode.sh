#!/bin/sh

#							##
#INSTALLS VSCODE. INSTRUCTIONS TAKEN FROM MICROSOFT SITE #
##							##

if [ "$EUID" -ne 0 ]
  then echo "Please run as root or run script with sudo..."
  exit
fi

# GRABBING REPO AND KEYS
rpm --import https://packages.microsoft.com/keys/microsoft.asc
sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

##
# UPDATING AND INSTALLING
##
dnf update
dnf install code -y
